import requests
import json

url = input("[+] Enter Link: ")
API_KEY="5d7be8b0f901254621a61caefd3d2fd182a1cf07"
URL=f"https://shrinkme.io/api?api={API_KEY}&url={url}"

response = requests.get(URL)
data = response.json()

if data["status"] == "success":
    print(f"[=] Url: {data['shortenedUrl']}")
else:
    raise Exception("Unable to mask given url")
